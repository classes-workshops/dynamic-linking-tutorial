/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * MIT License
 */

#pragma once

#include <iostream>

class IPrinter {
 public:
  virtual ~IPrinter() {}
  virtual void load(const std::string& msg) noexcept = 0;
  virtual const std::string& read() const noexcept = 0;
  virtual void print() const noexcept = 0;
};

/* Factories & Destroyers */
typedef IPrinter* IPrinterFactory();
typedef void IPrinterDestroyer(IPrinter*);
