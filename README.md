# Dynamic Objects

## Instructions

This project uses `meson` as building system. For installing meson, please, follow these steps:

```bash
sudo apt install ninja-build python3 python3-pip
sudo pip3 install meson
```

For compiling:

```bash
meson builddir
ninja -C builddir

# Objects available in the builddir folder
./builddir/main
./builddir/src/libprinter.so 
``` 

## Executing

For executing, only run main:

```bash
./builddir/main ./builddir/src/libprinter.so
```

It should load the `libprinter.so` object, create an abstract instance
specialised by dynamic linking and use the interface.

## Linting

Please, when modifying the code, keep the style:

```bash
clang-format -i --style=google *.cpp include/*.hpp src/*.cpp
```

## Authors

* MHPC. Luis G. Leon Vega <lleon95@gmail.com>
