/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * MIT License
 */

#include "iprinter.hpp"

class Printer : public IPrinter {
 public:
  virtual ~Printer() {}
  virtual void load(const std::string& msg) noexcept override;
  virtual const std::string& read() const noexcept override;
  virtual void print() const noexcept override;

 private:
  std::string msg_;
};

void Printer::load(const std::string& msg) noexcept { msg_ = msg; }

const std::string& Printer::read() const noexcept { return msg_; }

void Printer::print() const noexcept { std::cout << msg_ << std::endl; }

/**
 * Factories & Destroyers
 * dlopen does not support C++ mangling. The cast will be done outside
 */
extern "C" {
IPrinter* create_printer() { return new Printer; }

void destroy_printer(IPrinter* printer) { delete printer; }
}
