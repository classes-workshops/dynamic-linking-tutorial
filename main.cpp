/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * MIT License
 */

/* Based on: https://tldp.org/HOWTO/html_single/C++-dlopen/ */

#include <dlfcn.h> /* DL Library */

#include <iostream>
#include <memory>

#include "iprinter.hpp"

std::shared_ptr<IPrinter> PrinterFactory(void* printer_bin) {
  /* Load Factory */
  static IPrinterFactory* create_printer =
      reinterpret_cast<IPrinterFactory*>(dlsym(printer_bin, "create_printer"));
  const char* dlsym_error = dlerror();

  if (dlsym_error) {
    std::cerr << "Cannot load symbol create: " << dlsym_error << std::endl;
    return nullptr;
  }

  /* Load Destroyer */
  static IPrinterDestroyer* destroy_printer =
      reinterpret_cast<IPrinterDestroyer*>(
          dlsym(printer_bin, "destroy_printer"));
  dlsym_error = dlerror();
  if (dlsym_error) {
    std::cerr << "Cannot load symbol destroy: " << dlsym_error << std::endl;
    return nullptr;
  }

  /* Create ptr */
  return std::shared_ptr<IPrinter>(create_printer(), destroy_printer);
}

int main(int argc, char** argv) {
  /* Check parameters */
  if (argc <= 1) {
    std::cerr << "Error... Usage: <executable> <library.so>" << std::endl;
    return -1;
  }

  /* Load the library */
  void* printer_bin = dlopen(argv[1], RTLD_LAZY);
  if (!printer_bin) {
    std::cerr << "Cannot load library: " << dlerror() << std::endl;
    return 1;
  }
  /* Reset errors */
  dlerror();

  /* Create the instance */
  auto printer = PrinterFactory(printer_bin);
  if (!printer) {
    return 1;
  }

  /* Use the printer */
  printer->load("Hello World!");
  std::cout << "Read from printer: " << printer->read() << std::endl;
  std::cout << "Printer prints: " << std::endl;
  printer->print();

  /* Release the resources. IT SHOULD PERFORM BEFORE THE DLCLOSE */
  printer.reset();

  /* Close */
  dlclose(printer_bin);
  return 0;
}
